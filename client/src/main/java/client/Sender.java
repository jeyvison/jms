package client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Sender {
	
	@Autowired
	private JmsTemplate jmsTemplate;

	@RequestMapping("/send")
	public void send() {
		this.jmsTemplate.convertAndSend("testQueue", "Jeyvison");
	}

}
