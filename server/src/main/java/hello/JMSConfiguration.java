package hello;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.hornetq.api.core.TransportConfiguration;
import org.hornetq.core.remoting.impl.netty.NettyAcceptorFactory;
import org.springframework.boot.autoconfigure.jms.hornetq.HornetQConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@Configuration
public class JMSConfiguration {
	
	@Bean
	public HornetQConfigurationCustomizer hornetCustomizer() {
	    return new HornetQConfigurationCustomizer() {
	        @Override
	        public void customize(org.hornetq.core.config.Configuration configuration) {
	            Set<TransportConfiguration> acceptors = configuration.getAcceptorConfigurations();
	            Map<String, Object> params = new HashMap<String, Object>();
	            params.put("host", "localhost");
	            params.put("port", "5455");
	            TransportConfiguration tc = new TransportConfiguration(NettyAcceptorFactory.class.getName(), params);
	            acceptors.add(tc);
	        }
	    };
	}

}
